use bevy::prelude::*;

struct Player;
#[derive(Bundle)]
struct PlayerBundle
{
	_p: Player,

	#[bundle]
	sprite: SpriteBundle,
}

struct Materials
{
	player_material: Handle<ColorMaterial>,
	wall_material: Handle<ColorMaterial>,
}

fn main()
{
	App::build()
		.add_startup_system(setup.system())
		.add_startup_stage("player spawn", SystemStage::single(spawn_player.system()))
		.add_system(move_player.system())
		.add_plugins(DefaultPlugins)
		.run();
}

fn setup(mut commands: Commands, mut materials: ResMut<Assets<ColorMaterial>>)
{
	commands.spawn_bundle(OrthographicCameraBundle::new_2d());
	commands.insert_resource(Materials
	{
		player_material: materials.add(Color::rgb(1.0, 0.0, 1.0).into()),
		wall_material: materials.add(Color::rgb(0.0, 0.0, 0.0).into()),
	});
}

fn spawn_player(mut commands: Commands, materials: Res<Materials>)
{
	commands.spawn_bundle(PlayerBundle
	{
		_p: Player,
		sprite: SpriteBundle
		{
			material: materials.player_material.clone(),
			sprite: Sprite::new(Vec2::new(10.0, 10.0)),
			..Default::default()
		},
	});
}

fn move_player(keyboard_input: Res<Input<KeyCode>>, mut q: Query<&mut Transform, With<Player>>)
{
	let mut transform= q.single_mut()
		.expect("Should only be one player at a time");

	if keyboard_input.pressed(KeyCode::Left)
	{
		transform.translation.x -= 2.0;
	}
	if keyboard_input.pressed(KeyCode::Right)
	{
		transform.translation.x += 2.0;
	}
	if keyboard_input.pressed(KeyCode::Up)
	{
		transform.translation.y += 2.0;
	}
	if keyboard_input.pressed(KeyCode::Down)
	{
		transform.translation.y -= 2.0;
	}
}

fn spawn_maze(mut commands: Commands, materials: Res<Materials>)
{
	
}